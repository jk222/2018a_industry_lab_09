package ictgradschool.industry.lab09.ex03;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;


public class ex1 {

    public static void main(String[] args) {

        String[] arrayString = {"ONE", "TWO", "THREE", "FOUR"};
        ArrayList<String> arrayA = new ArrayList<>();
        Collections.addAll(arrayA, arrayString);


        for (int i = 0; i < arrayString.length; i++) {
            arrayA.set(i, arrayA.get(i).toLowerCase());
        }

        System.out.println(arrayA.toString());





        ArrayList<String> arrayB = new ArrayList<>();
        for (String element : arrayA) {
            arrayB.add(element.toLowerCase());

        }
        arrayA = arrayB;

        System.out.println(arrayA.toString());



        ArrayList<String> arrayC = new ArrayList<>();

        Iterator<String>myIterator = arrayA.iterator();
        while (myIterator.hasNext()) {
            String element = myIterator.next();
            arrayC.add(element.toLowerCase());
        }
        arrayA = arrayC;
        System.out.println(arrayA.toString());

    }
}






    /*

    EX1
        ArrayList<Point> list = new ArrayList<>();

        Point pt1 = new Point(3,4);
        System.out.println("pt1.x:" + pt1.x + " pt1.y: " + pt1.y);
        list.add(pt1);
        System.out.println(list.toString());

        Point pt2 = list.get(0);
        System.out.println("pt1.x:" + pt1.x + " pt1.y: " + pt1.y);
        System.out.println("pt2.x:" + pt2.x + " pt2.y: " + pt2.y);

        pt2.x = 23;
        System.out.println("pt1.x:" + pt1.x + " pt1.y: " + pt1.y);
        System.out.println("pt2.x:" + pt2.x + " pt2.y: " + pt2.y);

        if (pt2 == pt1){
            System.out.println("Same object");
        }else {
            System.out.println("Different object");
        }

    ArrayList<Character> list = new ArrayList<>();
    Character letter = new Character('a');

    System.out.println(letter.toString());

    list.add(letter);

    System.out.println(list.toString());

    if (list.get(0).equals("a")){
        System.out.println("funny");
    }else {
        System.out.println("Not funny");
    }


    ArrayList list = new ArrayList();
    list.add('a');
    list.add(0,34);

    //String c1 =(String) list.get(1);

String c2 = String.valueOf(list.get(1));
System.out.println(c2);

*/






